#include "header.hpp"
#include <boost/program_options.hpp>

namespace po = boost::program_options;

void conflicting_options(const boost::program_options::variables_map & vm,
                         const std::string & opt1, const std::string & opt2) {
  if (vm.count(opt1) && vm.count(opt2))
    throw std::logic_error(std::string("Conflicting options '") +
                          opt1 + "' and '" + opt2 + "'.");
}

ProgramOptions parse_arguments(int argc, const char *argv[]) {
  ProgramOptions options;
  options.stdin = false;

  try {
    po::options_description gen_op{"General options"};
    gen_op.add_options()
      ("help,h", "Help screen")
      ("file", po::value<std::string>(&options.file_name), "File name")
      ("stdin", "Read initial data from stream")
      ("search", po::value<std::string>(&options.search), ("Allowed  < " + dijkstra + " (dijkstra) | " + greedy + " (greedy) | " + star + " (star) > search algorithms").c_str())
      ("heuristic", po::value<std::string>(&options.heuristic), ("Allowed < " + manhattan + " (manhattan) | " + misplaced + " (misplaced) | " + euclidean + " (euclidean) > heuristic function").c_str());

    po::variables_map vm;
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(gen_op).allow_unregistered().run();
    po::store(parsed, vm);
    po::notify(vm);

    if ((size_t)(argc - 1) != vm.size()) {
      std::cout << RED << "Unrecognized arguments will be ignored :";
      for (int i = 1; i < argc; ++i) {
        if (argv[i][0] != '-')
          std::cout << " " << argv[i];
      }

      for (const auto& op : parsed.options) {
        if (op.unregistered)
          std::cout << " " << op.string_key;
      }
      std::cout << DEF << std::endl;
    }

    if (vm.count("help")) {
      std::cout << CYAN << gen_op << DEF << std::endl;
      exit(1);
    }

    conflicting_options(vm, "file", "stdin");

    { // check arguments
      if (vm.count("stdin"))
        options.stdin = true;

      if (vm.count("stdin") && vm.count("file"))
        throw std::logic_error("Using both arguments file and stdin - forbidden");

      if (vm.count("search") && (options.search != dijkstra && options.search != star && options.search != greedy)) {
          throw po::validation_error(po::validation_error::invalid_option_value, "search");
      }

      if (vm.count("heuristic") && (options.heuristic != manhattan && options.heuristic != misplaced && options.heuristic != euclidean)) {
          throw po::validation_error(po::validation_error::invalid_option_value, "heuristic");
      }
    }
  }
  catch (po::error &ex) {
    throw std::logic_error(ex.what());
  }

  return options;
}