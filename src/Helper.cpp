#include "Helper.hpp"

#include <algorithm>
#include <cmath>

bool Helper::is_solvable(const Board& board) {
  unsigned line_size = sqrt(board.size());
  BoardPtr target = Helper::generate_target(line_size);

  unsigned inversion = Helper::count_inversion(board);
  unsigned target_inversion = Helper::count_inversion(*target);

  if (line_size % 2 == 0) {
    inversion += (std::find(board.begin(), board.end(), 0) - board.begin()) / line_size;
    target_inversion += (std::find(target->begin(), target->end(), 0) - target->begin()) / line_size;
  }

  return (inversion % 2) == (target_inversion % 2);
}

std::string Helper::to_string(const Board& board, const std::string& color) {
  std::string res = color;
  unsigned size = board.size();
  unsigned line_size = sqrt(size);
  unsigned pres = std::to_string(size -1).size();

  for (unsigned i = 0; i < size; ++i) {
    std::string s_num = std::to_string(board[i]);

    for (unsigned j = 0; j < (pres - s_num.size()); ++j) {
      res.push_back(' ');
    }

    if (board[i] == 0) {
      res += RED + s_num + color;
    } else {
      res += s_num;
    }

    if ((i + 1) % line_size == 0)
      res += "\n";
    else
      res += " ";
  }
  res += DEF;

  return res;
}

unsigned Helper::count_inversion(const Board& board) {
  unsigned inversion = 0;
  unsigned size = board.size();

  for (unsigned i = 0; i < size; ++i) {
    if (board[i] == 0) {
        continue;
    }

    for (unsigned j = i + 1; j < size; ++j) {
      if (board[j] == 0)
        continue;

      if (board[i] > board[j])
        ++inversion;
    }
  }

  return inversion;
}

BoardPtr Helper::generate_target(unsigned size) {
  Board target;
  for (unsigned i = 0; i < (size * size); ++i) {
    target.push_back(0);
  }

  int x_min = 0;
  int x_max = size;

  int y_min = 0;
  int y_max = size;

  unsigned num = 0;
  auto num_gen = [&num, size]() {
    ++num;
    return (num == (size * size)) ? 0 : num;
  };

  while (x_min < x_max && y_min < y_max) {
    for (int i = x_min; i < x_max; ++i)
      target[(y_min * size) + i] = num_gen();
    ++y_min;

    for (int i = y_min; i < y_max; ++i)
      target[(i * size) + x_max - 1] = num_gen();
    --x_max;

    for (int i = x_max - 1; i >= x_min; --i)
      target[(y_max -1) * size + i] = num_gen();
    --y_max;

    for (int i = y_max - 1; i >= y_min; --i)
      target[(i * size) + x_min] = num_gen();
    ++x_min;
  }

  target.shrink_to_fit();
  return std::make_shared<Board>(target);
}

bool Helper::is_final(const Board& board, const Board& target) {
  return board == target;
}
