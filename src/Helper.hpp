#ifndef HELPER_HPP
#define HELPER_HPP

#include "header.hpp"

#include <string>
#include <memory>

class Helper {
public:
  static bool         is_solvable(const Board& board);
  static BoardPtr     generate_target(unsigned size);
  static std::string  to_string(const Board& board, const std::string& color);
  static bool         is_final(const Board& board, const Board& target);

private:
  static unsigned     count_inversion(const Board& board);
};

#endif //HELPER_HPP