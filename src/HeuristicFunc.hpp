#ifndef HEURISTICFUNC_HPP
#define HEURISTICFUNC_HPP

#include "header.hpp"

typedef unsigned (*HFunc)(const Board&, const Board&, unsigned);

class HeuristicFunc {
public:
  static unsigned getManhattan(const Board& board, const Board& target, unsigned line_size);
  static unsigned getMisplaced(const Board& board, const Board& target, unsigned line_size);
  static unsigned getEuclideanDistance(const Board& board, const Board& target, unsigned line_size);
};


#endif // HEURISTICFUNC_HPP