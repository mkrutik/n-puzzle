#ifndef HEADER_HPP
#define HEADER_HPP

#include <iostream>
#include <cmath>

#include <string>
#include <vector>
#include <memory>

// terminal colors
#define DEF     "\e[0;0m"
#define GREEN   "\e[1;32m"
#define RED     "\e[1;31m"
#define YELLOW  "\e[1;33m"
#define CYAN    "\e[0;35m"

// program arguments
const std::string dijkstra = "d";
const std::string greedy = "g";
const std::string star = "s";

const std::string manhattan = "m";
const std::string misplaced = "mi";
const std::string euclidean = "e";

typedef struct {
  bool        stdin;
  std::string file_name;
  std::string search;
  std::string heuristic;
} ProgramOptions;

typedef std::vector<unsigned> Board;
typedef std::shared_ptr<Board> BoardPtr;

const char COMMENT = '#';

ProgramOptions parse_arguments(int argc, const char *argv[]);
Board generate_initial(void);


std::vector<std::string> read_file(const std::string& file_name);
std::vector<std::string> read_stream(void);

Board parse_and_validate(const std::vector<std::string>& data);

#endif // HEADER_HPP