#include "header.hpp"

#include <regex>
#include <map>

Board parse_and_validate(const std::vector<std::string>& data) {
  if (data.size() < 1)
    throw std::runtime_error("Invalid content : absent line with map size");

  const std::string first_line_reg = "^(\\d{1,10})(\\s*)$";
  if (!std::regex_match(data.at(0), std::regex(first_line_reg)))
    throw std::runtime_error("Invalid content : invalid size line");

  unsigned size = std::stoul(data.at(0));
  Board board;

  if ((data.size() - 1) != size)
    throw std::runtime_error("Invalid content : required size: " + std::to_string(size) + "x" + std::to_string(size));

  std::map<unsigned, unsigned> for_check;
  const std::string rest_lines_reg = "^\\s*((\\d{1,10}\\s+){" + std::to_string(size) + "})$";
  for (unsigned i = 1; i < data.size(); ++i) {
    std::string tmp = data.at(i);
    tmp.push_back(' '); // kostyl for regexp

    if (!std::regex_match(tmp, std::regex(rest_lines_reg)))
      throw std::runtime_error("Invalid line content : wrong line content");

    std::stringstream ss(tmp);
    std::string line;
    for (unsigned j = 0; ss >> line; ++j) {
      unsigned num = std::stoul(line);

      if (for_check.find(num) != for_check.end())
        throw std::runtime_error("Invalid line content : several identical tiles in input");
      else if (num >= (size * size))
        throw std::runtime_error("Invalid line content : tile number out of range");
      else
        for_check.insert(std::pair<unsigned, unsigned>(num, num));

      board.push_back(num);
    }
  }

  board.shrink_to_fit();
  return board;
}