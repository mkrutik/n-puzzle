#include "HeuristicFunc.hpp"

#include <algorithm>
#include <cmath>

unsigned HeuristicFunc::getManhattan(const Board& board, const Board& target, unsigned line_size) {
  unsigned distance = 0;

  for (unsigned index = 0; index < board.size(); ++index) {
    int x = static_cast<int>(index % line_size);
    int y = static_cast<int>(index / line_size);

    unsigned target_index = std::find(target.begin(), target.end(), board[index]) - target.begin();
    int t_x = static_cast<int>(target_index % line_size);
    int t_y = static_cast<int>(target_index / line_size);

	  distance += abs(t_x - x) + abs(t_y - y);
  }

  return distance;
}

unsigned HeuristicFunc::getMisplaced(const Board& board, const Board& target, unsigned line_size) {
  unsigned distance = 0;

  (void)line_size;

  for (unsigned index = 0; index < board.size(); ++index) {
    if (board[index] != target[index])
      ++distance;
  }

  return distance;
}


unsigned HeuristicFunc::getEuclideanDistance(const Board& board, const Board& target, unsigned line_size) {
  unsigned distance = 0;

  for (unsigned index = 0; index < board.size(); ++index) {
    int x = static_cast<int>(index % line_size);
    int y = static_cast<int>(index / line_size);

    unsigned target_index = std::find(target.begin(), target.end(), board[index]) - target.begin();
    int t_x = static_cast<int>(target_index % line_size);
    int t_y = static_cast<int>(target_index / line_size);

	  distance += static_cast<unsigned>(pow(t_x - x, 2) + pow(t_y - y, 2));
  }

  return distance;
}