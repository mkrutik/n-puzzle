#ifndef SOLVER_HPP
#define SOLVER_HPP

#include "Helper.hpp"
#include "HeuristicFunc.hpp"
#include <chrono>
#include <queue>
#include <set>

typedef struct State {
  Board    board;
  unsigned zero_index;
  unsigned g_cost;
  unsigned h_cost;
  unsigned priority;

  Board    prev;
} State;

struct Compare
{
  bool operator() (const State& l, const State& r) const {
    return l.priority > r.priority;
  }
};

typedef std::priority_queue<State, std::vector<State>, Compare > PriorityQueue;
class OpenPriorityQueue : public PriorityQueue {
public:
  std::vector<State>::iterator begin() {
    return OpenPriorityQueue::c.begin();
  }

  std::vector<State>::iterator end() {
    return OpenPriorityQueue::c.end();
  }

  void update(void) {
    std::make_heap(this->c.begin(), this->c.end(), this->comp);
  }

};


class Solver {
public:

  enum SearchAlgorithm {
    DijkstraSearch = 0,
    GreedySearch   = 1,
    StarSearch     = 2
  };

  Solver(const Board& initial, const Board& target, unsigned line_size,
        HFunc func = HeuristicFunc::getManhattan, SearchAlgorithm search = StarSearch);
  void solve(void);

private:
  void  main_loop(void);
  std::vector<State> expand(void);
  void  print_info(void);
  std::vector<Board> get_path(void);

  typedef void (Solver::*AddNeighbors)(std::vector<State>& neighbors);
  void  star_adding_neighbors(std::vector<State>& neighbors);
  void  greedy_adding_neighbors(std::vector<State>& neighbors);
  void  dijkstra_adding_neighbors(std::vector<State>& neighbors);

  const Board&     _initial;
  const Board&     _target;
  const unsigned   _line_size;
  HFunc            _heuristic_func;
  SearchAlgorithm  _search_type;
  AddNeighbors     _add_neighbors;

  OpenPriorityQueue _open;
  std::set<State>   _close;
  State             _curr;
  bool              _solved;

  unsigned                  _number_of_moves;
  unsigned                  _complexity_time;
  unsigned                  _complexity_size;
  std::chrono::nanoseconds  _time;

  bool _explain;
};


#endif //SOLVER_HPP