#include "Solver.hpp"

#include <algorithm>
#include <functional>
#include <chrono>
#include <iomanip>
#include <thread>

bool operator < (const State& l, const State& r) {
  return l.board < r.board;
}

std::ostream& operator << (std::ostream& os, std::chrono::nanoseconds ns)
{
    using namespace std;
    using namespace std::chrono;
    typedef duration<int, ratio<86400>> days;
    char fill = os.fill();
    os.fill('0');
    auto d = duration_cast<days>(ns);
    ns -= d;
    auto h = duration_cast<hours>(ns);
    ns -= h;
    auto m = duration_cast<minutes>(ns);
    ns -= m;
    auto s = duration_cast<seconds>(ns);
    ns -= s;
    auto ms = duration_cast<milliseconds>(ns);
    ns -= ms;
    auto mc = duration_cast<microseconds>(ns);
    ns -= mc;


    os << std::setw(2) << d.count() << "d: "
       << std::setw(2) << h.count() << "h: "
       << std::setw(2) << m.count() << "m: "
       << std::setw(2) << s.count() << "s: "
       << std::setw(2) << ms.count() << "ms: "
       << std::setw(2) << mc.count() << "mc: "
       << std::setw(2) << ns.count() << "ns";
    os.fill(fill);
    return os;
};


Solver::Solver(const Board& initial, const Board& target, unsigned line_size,
              HFunc func, SearchAlgorithm search) :
  _initial(initial),
  _target(target),
  _line_size(line_size),
  _heuristic_func(func),
  _solved(false),
  _complexity_time(1),
  _complexity_size(0) {

  if (search == Solver::SearchAlgorithm::StarSearch)
    _add_neighbors = &Solver::star_adding_neighbors;
  else if (search == Solver::SearchAlgorithm::DijkstraSearch)
    _add_neighbors = &Solver::dijkstra_adding_neighbors;
  else
    _add_neighbors = &Solver::greedy_adding_neighbors;

  _explain = false;
}


void  Solver::star_adding_neighbors(std::vector<State>& neighbors) {

  for (auto& s : neighbors) {
    s.g_cost = _curr.g_cost + 1;

    auto find_in_close = _close.find(s);
    if (find_in_close != _close.end()) {
      if (s.g_cost < find_in_close->g_cost) {
        _close.erase(find_in_close);

        s.h_cost = find_in_close->h_cost;
        s.priority = s.g_cost + s.h_cost;
        s.prev = _curr.board;

        _open.push(s);
      }

      continue;
    }

    auto find_in_open = std::find_if(_open.begin(), _open.end(), [&s](const State& elem) { return elem.board == s.board; });
    if (find_in_open != _open.end()) {
      if (s.g_cost < find_in_open->g_cost) {
        find_in_open->g_cost = s.g_cost;
        find_in_open->priority = find_in_open->g_cost + find_in_open->h_cost;
        find_in_open->prev = _curr.board;

        _open.update();
      }

      continue;
    }

    s.h_cost = _heuristic_func(s.board, _target, _line_size);
    s.priority = s.g_cost + s.h_cost;
    s.prev = _curr.board;

    _open.push(s);
  }
}


void Solver::greedy_adding_neighbors(std::vector<State>& neighbors) {
  for (auto& s : neighbors) {

    auto find_in_close = _close.find(s);
    if (find_in_close != _close.end())
      continue;

    auto find_in_open = std::find_if(_open.begin(), _open.end(), [&s](const State& elem) { return elem.board == s.board; });
    if (find_in_open != _open.end())
      continue;

    s.h_cost = _heuristic_func(s.board, _target, _line_size);
    s.priority = s.h_cost;
    s.prev = _curr.board;
    _open.push(s);
  }
}


void Solver::dijkstra_adding_neighbors(std::vector<State>& neighbors) {
  for (auto& s : neighbors) {

    auto find_in_close = _close.find(s);
    if (find_in_close != _close.end())
      continue;

    auto find_in_open = std::find_if(_open.begin(), _open.end(), [&s](const State& elem) { return elem.board == s.board; });
    if (find_in_open != _open.end())
      continue;

    if (Helper::is_final(s.board, _target)) {
      s.prev = _curr.board;
      _curr = s;
      _solved = true;
      break;
    }

    s.g_cost = _curr.g_cost + 1;
    s.priority = s.g_cost;
    s.prev = _curr.board;
    _open.push(s);
  }
}


void Solver::solve(void) {
  main_loop();
}

void  Solver::main_loop(void) {
  OpenPriorityQueue open;
  std::set<State>   close;

  unsigned zero_index = std::find(_initial.begin(), _initial.end(), 0) - _initial.begin();
  _open.push({_initial, zero_index, 0, 0, 0, {}});

  auto start_time = std::chrono::steady_clock::now();

  while (!_solved && !_open.empty()) {
    _curr = _open.top();
    ++_complexity_time;

    // found solution
    if (Helper::is_final(_curr.board, _target))
      break;

    _open.pop();
    _close.insert(_curr);

    // Maximum number of states ever represented in memory at the same time during the search (complexity in size)
    if ((_open.size() + _close.size()) > _complexity_size)
      _complexity_size = _open.size() + _close.size();

    std::vector<State> neighbors = expand();
    (this->*_add_neighbors)(neighbors);
  }

  auto end_time = std::chrono::steady_clock::now();
  _time = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);

  print_info();
}


std::vector<State> Solver::expand(void) {
  std::vector<State> res;

  unsigned zero_index = _curr.zero_index;
  unsigned x_zero = zero_index % _line_size;
  unsigned y_zero = zero_index / _line_size;

  // LEFT
  if (x_zero != 0) {
    State left{_curr.board, (zero_index - 1), 0, 0, 0, {}};
    left.board[zero_index] = left.board[zero_index - 1];
    left.board[zero_index - 1] = 0;

    res.push_back(left);
  }

  // RIGHT
  if (x_zero != (_line_size - 1)) {
    State right{_curr.board, (zero_index + 1), 0, 0, 0, {}};;
    right.board[zero_index] = right.board[zero_index + 1];
    right.board[zero_index + 1] = 0;

    res.push_back(right);
  }

  // UP
  if (y_zero != 0) {
    State up{_curr.board, (zero_index - _line_size), 0, 0, 0, {}};;
    up.board[zero_index] = up.board[zero_index - _line_size];
    up.board[zero_index - _line_size] = 0;

    res.push_back(up);
  }

  // DOWN
  if (y_zero != (_line_size - 1)) {
    State down{_curr.board, (zero_index + _line_size), 0, 0, 0, {}};;
    down.board[zero_index] = down.board[zero_index + _line_size];
    down.board[zero_index + _line_size] = 0;

    res.push_back(down);
  }

  return res;
}

std::vector<Board> Solver::get_path(void) {
  std::vector<Board> res;

  res.push_back(_curr.board);

  State* tmp = &_curr;
  while (!tmp->prev.empty()) {
    res.push_back(tmp->prev);
    *tmp = *(_close.find({tmp->prev,0,0,0,0,{}}));
  }

  std::reverse(res.begin(), res.end());

  return res;
}

void  Solver::print_info(void) {
  std::vector<Board> path = get_path();
  _number_of_moves = path.size() - 2;

  std::cout << GREEN << "Spent time : " << _time << std::endl
            << CYAN << "Number of moves: " << _number_of_moves << std::endl
            << "Complexity in time : " << _complexity_time << std::endl
            << "Complexity in size : " << _complexity_size << std::endl << std::endl;

  std::this_thread::sleep_for(std::chrono::seconds(2));

  for (unsigned i = 0; i < path.size(); ++i) {
    if (i == path.size() - 1) {
      std::cout << Helper::to_string(path[i], GREEN) << std::endl;
    } else if (i != 0) {
      std::cout << CYAN << "Move number " << i << std::endl;
      std::cout << Helper::to_string(path[i], YELLOW) << std::endl;
    } else {
      std::cout << Helper::to_string(path[i], YELLOW) << std::endl;
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
  std::cout << GREEN << "The end :)" <<  DEF << std::endl;
}