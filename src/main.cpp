#include "header.hpp"

#include "Helper.hpp"
#include "Solver.hpp"
#include <map>



void print_error(const std::string& message) {
  std::cout << RED << "Error: " << message <<  DEF << std::endl;
}

int main(int argc, const char *argv[]) {
  std::vector<std::string> data;
  try {
    ProgramOptions options = parse_arguments(argc, argv);
    std::map<std::string, Solver::SearchAlgorithm> searchs = {{"d", Solver::DijkstraSearch}, {"g", Solver::GreedySearch}, {"s", Solver::StarSearch}, {"", Solver::GreedySearch}};
    std::map<std::string, HFunc> heur = {{"m", &HeuristicFunc::getManhattan}, {"e", &HeuristicFunc::getEuclideanDistance}, {"mi", &HeuristicFunc::getMisplaced}, {"", &HeuristicFunc::getManhattan}};


    Board initial;

    if (options.stdin) {
      data = read_stream();
      initial = parse_and_validate(data);
    }
    else if (!options.file_name.empty()) {
      data = read_file(options.file_name);
      initial = parse_and_validate(data);
    }
    else
      initial = generate_initial();

    unsigned size = static_cast<unsigned>(sqrt(initial.size()));
    BoardPtr target = Helper::generate_target(size);

    bool is_solvable = Helper::is_solvable(initial);

    std::cout << CYAN << "Puzzle size: " << GREEN << size << std::endl << std::endl
              << CYAN << "Initial state:" << std::endl
              << YELLOW << Helper::to_string(initial, YELLOW) << std::endl
              << CYAN << "Target state:" << std::endl
              << GREEN << Helper::to_string(*target, GREEN)
              << DEF << std::endl;

    if (is_solvable) {
      std::cout << GREEN << "Pazzle SOLVABLE" << DEF << std::endl << std::endl
                << CYAN << "********************* Soving *********************" << std::endl;

      Solver solver{initial, *target, size, heur.find(options.heuristic)->second, searchs.find(options.search)->second};
      solver.solve();


    } else {
      std::cout << RED << "Puzzle UNSOLVABLE" << DEF << std::endl;
    }


  } catch (std::exception &e) {
    print_error(e.what());
  }

  return (0);
}
