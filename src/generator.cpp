#include "header.hpp"
#include "Helper.hpp"

#include <ctime>
#include <cstdlib>
#include <cmath>

Board generate_initial(void) {
  std::srand(unsigned(std::time(0)));
  int random_variable = 3 + (std::rand() % (4 - 3 + 1));

  // size 3-4
  BoardPtr res = Helper::generate_target(random_variable);
  unsigned line_size = static_cast<unsigned>(sqrt(res->size()));

  // 30-80 permutations
  random_variable = 30 + (std::rand() % (70 - 30 + 1));
  while (random_variable > 0 || !Helper::is_solvable(*res)) {
    int index = std::rand() % res->size();
    unsigned tmp;

    // horizontal
    if (std::rand() % 2 == 0) {
      if (index % line_size > 0) {
        tmp = (*res)[index];
        (*res)[index] = (*res)[index - 1];
        (*res)[index - 1] = tmp;
      } else {
        tmp = (*res)[index];
        (*res)[index] = (*res)[index + 1];
        (*res)[index + 1] = tmp;
      }

    } else { //vertiacal
      if (index / line_size > 0) {
        tmp = (*res)[index];
        (*res)[index] = (*res)[index - line_size];
        (*res)[index - line_size] = tmp;
      } else {
        tmp = (*res)[index];
        (*res)[index] = (*res)[index + line_size];
        (*res)[index + line_size] = tmp;
      }
    }

    --random_variable;
  }

  return *res;
}
