#include "header.hpp"

#include <fstream>

#include <streambuf>
#include <sstream>
#include <algorithm>

std::vector<std::string> split_to_lines(const std::string& data) {
  std::vector<std::string> res;

  std::istringstream inss(data);
  std::string line;
  while (std::getline(inss, line))
  {
    if (line.empty())
      continue;

    std::string::iterator it = std::find_if(line.begin(), line.end(), [](char c ) {return !std::isspace(c);} );
    if (it == line.end())
      continue;

    size_t pos = line.find(COMMENT);
    if (pos == std::string::npos) {
      res.push_back(line);

    } else if (pos != 0) {
      std::string tmp = line.substr(0, pos);

      it = std::find_if(tmp.begin(), tmp.end(), [](char c ) {return !std::isspace(c);} );
      if (it != tmp.end())
        res.push_back(tmp);
    }
  }

  return res;
}

std::vector<std::string> read_file(const std::string& file_name) {
  try {
    std::ifstream file(file_name);

    if (!file.is_open())
      throw std::runtime_error("Error: can`t open the file " + file_name);

    std::string data((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    std::vector<std::string> res = split_to_lines(data);
    return res;

  } catch(std::exception& e) {
    throw std::runtime_error("Error: can`t open the file " + file_name);
  }
  return {};
}

std::vector<std::string> read_stream(void) {
  std::vector<std::string> res;
  std::string tmp;

  for (std::string line; std::getline(std::cin, line);) {
    tmp += line + "\n";
  }

  res = split_to_lines(tmp);
  return res;
}